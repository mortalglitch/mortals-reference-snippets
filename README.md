# Mortals-Reference-Snippets

A collection of snippets to help quickly get me back into projects.
This is more or less going to be a space where I can dump notes related to various programming language and development tools.
I cannot guarantee if these files will be helpful for anyone aside from myself. If I get a chance I will go back and clean these up as I can.

## Project status
Though most individual items will build in this project none of it is intended to be a stand alone working application.

## License
Not intended to have any sort of license. These files act as my notes for various languages and development tools. Feel free to use anything you deem helpful.
If this grows into something more and you feel I should license items please file an issue and we can discuss.




