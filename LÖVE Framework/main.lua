-- Much of the code contained here in is based on the tutorial from
-- https://sheepolution.com/learn/book/contents
-- As of 01/11/2022 (dd-mm-yyyy)

-- This is a comment. Highlighted by the "--" at the beginning.

-- Below is a print function which allows data to be posted to the console.
print(123)

-- Variables and how they can be used and intertwined.
name = "mortal"
location = "The Internet"
text = "Hello, my name is " .. name .. ", and I'm from " .. location .. "."
print(text)

-- Variables can't start with a number
-- 8test == error
-- Variable names also cannot contain special characters
name_1 = "Test"
print(name_1)

-- Variable can self add
coins = 0
coins = coins + 5
print(coins)

-- Functions
function example()
  print("Hello World")
end

-- Call said function
example()

-- Parameters (pass data to a function)
function sayThing(thing)
  print("I love " .. thing)
end

sayThing("lamp")
sayThing("table")
sayThing("door")
sayThing("floor")

-- Return Functions (returns a value to the requestor)
function set_five()
  return 5
end

a = set_five()
print(a)

-- Most of the above has been lua
-- love functions have a flow to them
-- love.load -> love.update -> love.draw -> love.update -> love.draw (on repeat)
function love.load()
  x_rec = 100
  
  -- Table variable storage
  -- insert adds new variable to back of the blob
  -- remove pulls out x position and move the later variables up (3 becomes 2 etc.)
  -- can be overwritten via direct positional reference
  penguins = {"bob", "francis"}
  table.insert(penguins, "terry")
  table.insert(penguins, "monte carlo")
  table.remove(penguins, 2)
  penguins[1] = "bobert"
end

function love.draw()
  love.graphics.print("Hello World", 100, 100)
  -- For loop and table reference
  -- #penguins returns the count in the penguins table
  for i=1, #penguins do
    love.graphics.print(penguins[i], 200, 200 + 14 * i)
  end
  -- ipairs is another shorthand to iterate through a table
  for i, png in ipairs(penguins) do
    love.graphics.print(png, 300, 300 + 14 * i)
  end

  love.graphics.rectangle("line", x_rec, 50, 200, 150)
end

function love.update(dt)
  -- If/Then comparison
  --if x_rec < 600 then
    --x_rec = x_rec + 100 * dt
  --end
  if love.keyboard.isDown("right") then
    x_rec = x_rec + 150 * dt
  elseif love.keyboard.isDown("left") then
    x_rec = x_rec - 150 * dt
  end
end

